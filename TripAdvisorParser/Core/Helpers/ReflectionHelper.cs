﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace TripAdvisorParser.Core.Helpers
{
    public static class ReflectionHelper
    {
        /// <summary>
        /// Получает аттрибут Description у заданного типа.
        /// </summary>
        /// <param name="type">Тип, у которого необходимо получить описание.</param>
        /// <returns>Аттрибут Description заданного типа</returns>
        public static string GetDescription(Type type)
        {
            var descriptions = (DescriptionAttribute[])
                type.GetCustomAttributes(typeof(DescriptionAttribute), false);
            
            return descriptions.Length == 0 ? null : descriptions[0].Description;
        }

        /// <summary>
        /// Получает типы, которые наследуются от заданного.
        /// </summary>
        /// <param name="type">Наследуемый тип</param>
        /// <returns>Типы, которые наследуются от заданного.</returns>
        public static IEnumerable<Type> GetInheritedTypes(Type type) 
            => AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(t => type.IsAssignableFrom(t)
                            && !t.IsInterface
                            && !t.IsAbstract);
    }
}