﻿using System;

namespace TripAdvisorParser.Core.Helpers
{
    public static class UrlHelper
    {
        /// <summary>
        /// Проверяет, совпадают ли ссылки.
        /// </summary>
        /// <param name="url1">Ссылка 1</param>
        /// <param name="url2">Ссылка 2</param>
        /// <returns>True, если ссылки совпадают</returns>
        public static bool IsEqualsUrls(string url1, string url2)
        {
            var uri1 = new Uri(url1);
            var uri2 = new Uri(url2);

            var result = Uri.Compare(uri1, uri2, 
                UriComponents.Host | UriComponents.PathAndQuery, 
                UriFormat.SafeUnescaped, StringComparison.OrdinalIgnoreCase);

            return result == 0;
        }
    }
}