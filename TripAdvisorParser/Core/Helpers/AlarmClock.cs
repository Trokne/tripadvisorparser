﻿// Класс взят из интернета: https://stackoverflow.com/questions/1493203/alarm-clock-application-in-net

using System;
using System.Timers;

namespace TripAdvisorParser.Core.Helpers
{
    public class AlarmClock
    {
        #region Приватные переменные

        private readonly Timer _timer;
        private readonly DateTime _alarmTime;
        private bool _isEnabled;

        #endregion

        #region Событие

        public event EventHandler Alarm;

        private void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (!_isEnabled || DateTime.Now <= _alarmTime) 
                return;
            
            _isEnabled = false;
            OnAlarm();
            _timer.Stop();
        }
        
        protected virtual void OnAlarm()
        {
            Alarm?.Invoke(this, EventArgs.Empty);
        }

        #endregion
        
        #region Инициализация и уничтожение

        public AlarmClock(DateTime alarmTime)
        {
            _alarmTime = alarmTime;

            _timer = new Timer();
            _timer.Elapsed += timer_Elapsed;
            _timer.Interval = 1000;
            _timer.Start();

            _isEnabled = true;
        }

        #endregion
    }
}