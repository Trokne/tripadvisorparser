﻿namespace TripAdvisorParser.Core.Helpers
{
    /// <summary>
    /// Реализация синглтона.
    /// </summary>
    /// <typeparam name="T">Класс, реализующий синглтон.</typeparam>
    public abstract class Singleton<T> where T : new()
    {
        private static T _instance;
        
        public static T Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new T();
                return _instance;
            }
            protected set => _instance = value;
        }
        
        
    }
}