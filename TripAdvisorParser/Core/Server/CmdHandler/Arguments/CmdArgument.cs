﻿using System;

namespace TripAdvisorParser.Core.Server.CmdHandler
{
    public abstract class CmdArgument
    {
        /// <summary>
        /// Инициализирует новый аргумент командной строки.
        /// </summary>
        /// <param name="command">Команда в CMD, которая является правильной для вызова метода <see cref="action"/></param>
        /// <param name="helpDescription">Описание команды</param>
        /// <param name="action">Действие, выполняемое после корректного считывания команды</param>
        protected CmdArgument(string command, string helpDescription)
        {
            Command = command;
            HelpDescription = helpDescription;
        }

        /// <summary>
        /// Команда в CMD, которая является правильной для вызова <see cref="Action"/>
        /// </summary>
        public string Command { get; set; }
        /// <summary>
        /// Подсказка, выводимая в коносль при help
        /// </summary>  
        public string HelpDescription { get; set; }

        /// <summary>
        /// Выводит подсказку о команде.
        /// </summary>
        public void PrintHelp() => InputOutput.Write($"{Command.ToUpper()}\t{HelpDescription}");
        
        /// <summary>
        /// Показывает значение по умолчанию, если вызов команды не произойдёт.
        /// </summary>
        public abstract string GetDefaultInvokation();

    }
}