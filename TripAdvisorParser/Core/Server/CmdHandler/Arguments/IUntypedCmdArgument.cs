﻿namespace TripAdvisorParser.Core.Server.CmdHandler.Arguments
{
    public interface IUntypedCmdArgument
    {
        void InvokeAction();
    }
}