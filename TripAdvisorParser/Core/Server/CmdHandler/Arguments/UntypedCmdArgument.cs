﻿using System;
using TripAdvisorParser.Core.Data.Api;
using TripAdvisorParser.Core.Data.Api.Commands;
using TripAdvisorParser.Core.Server.CmdHandler.Arguments;

namespace TripAdvisorParser.Core.Server.CmdHandler
{
    public class UntypedCmdArgument : CmdArgument, IUntypedCmdArgument
    {
        /// <summary>
        /// Действие, выполняемое после корректного считывания <see cref="Command"/>
        /// </summary>
        private readonly Action _action;
        
        /// <inheritdoc />
        public UntypedCmdArgument(string command, string helpDescription, Action action) : base(command, helpDescription)
        {
            _action = action;
        }
        
        /// <summary>
        /// Выполняет действие, связанное с заданным аргументом командной строки.
        /// </summary>
        public void InvokeAction()
        {
            InputOutput.Write($"Вызван аргумент командной строки: {Command}", ConsoleColor.DarkYellow);
            _action.Invoke();
        }

        /// <inheritdoc />
        public override string GetDefaultInvokation()
            =>  $"[{Command}]";
    }
}