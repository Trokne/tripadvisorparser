﻿using System;
using System.ComponentModel;
using TripAdvisorParser.Core.Data.Api;
using TripAdvisorParser.Core.Data.Api.Commands;
using TripAdvisorParser.Core.Server.CmdHandler.Arguments;

namespace TripAdvisorParser.Core.Server.CmdHandler
{
    public class TypedCmdCmdArgument<T> : CmdArgument, ITypedCmdArgument
    {
        /// <summary>
        /// Действие, выполняемое после корректного считывания <see cref="Command"/>
        /// </summary>
        private readonly Action<T> _action;
        /// <summary>
        /// Значение по умолчанию.
        /// </summary>
        private readonly T _defaultValue;

        /// <inheritdoc />
        public TypedCmdCmdArgument(string command, string helpDescription, Action<T> action, T defaultValue) 
            : base(command, helpDescription)
        {
            _action = action;
            _defaultValue = defaultValue;
        }
        
        /// <summary>
        /// Выполняет действие, связанное с заданным аргументом командной строки.
        /// </summary>
        /// <param name="arg">Аргумент командной строки (не команда!)</param>
        public void InvokeAction(string arg)
        {
            if (!TryConvert(out var param, arg)) 
                return;
            InputOutput.Write($"Вызван аргумент командной строки {Command.ToUpper()} со значением: {arg}", ConsoleColor.DarkYellow);
            _action.Invoke(param);
        }
        
        /// <summary>
        /// Пробует преобразовать поступившее в команде значение в тип <see cref="T"/>
        /// </summary>
        /// <param name="variable">Преобразованное значение</param>
        /// <param name="input">Поступившее значение из CMD</param>
        /// <returns>True, если преобразование удалось</returns>
        private static bool TryConvert(out T variable, string input)
        {
            try
            {
                var converter = TypeDescriptor.GetConverter(typeof(T));
                variable = (T)converter.ConvertFromString(input);
                return true;
            }
            catch (NotSupportedException e)
            {
                InputOutput.Write(e);
                variable = default;
                return false;
            }
        }

        /// <inheritdoc />
        public override string GetDefaultInvokation()
            => $"[{Command} {_defaultValue.ToString()}]";
    }
}