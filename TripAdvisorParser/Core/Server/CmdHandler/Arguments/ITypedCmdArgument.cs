﻿namespace TripAdvisorParser.Core.Server.CmdHandler.Arguments
{
    public interface ITypedCmdArgument
    {
        void InvokeAction(string param);
    }
}