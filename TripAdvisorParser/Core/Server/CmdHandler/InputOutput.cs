﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using CsQuery.ExtensionMethods;
using CsQuery.Output;
using TripAdvisorParser.Core.Data;
using TripAdvisorParser.Core.Data.Parameters;
using TripAdvisorParser.Core.Server.CmdHandler.Arguments;

namespace TripAdvisorParser.Core.Server.CmdHandler
{
    /// <summary>
    /// Отвечает за ввод/вывод информации в консоли.
    /// </summary>
    public static class InputOutput
    {
        #region Приватные переменные

        /// <summary>
        /// Стандартный цвет фона сообщения в консоли.
        /// </summary>
        private static readonly ConsoleColor DefaultBackgroundColor;
        /// <summary>
        /// Стандартный цвет текста сообщения в консоли.
        /// </summary>
        private static readonly ConsoleColor DefaultForegroundColor;
        /// <summary>
        /// Возможные аргументы командной строки.
        /// </summary>
        private static HashSet<CmdArgument> _cmdArguments;

        #endregion

        #region Инициализация

        static InputOutput()
        {
            DefaultForegroundColor = Console.ForegroundColor;
            DefaultBackgroundColor = Console.BackgroundColor;
            InitializeCommandLineArguments();
        }

        /// <summary>
        /// Инициализирует аргументы командной строки.
        /// </summary>
        private static void InitializeCommandLineArguments()
        {
            _cmdArguments = new HashSet<CmdArgument>
            {
                new UntypedCmdArgument("-help", "Выводит подсказку", () =>
                {
                    _cmdArguments.ForEach(c => c.PrintHelp()); 
                    Environment.Exit(Environment.ExitCode);
                }),
                new TypedCmdCmdArgument<int>("-port", "Устанавливает порт сервера", x => Settings.Instance.General.StartPort = x, Settings.Instance.General.StartPort),
                new TypedCmdCmdArgument<int>("-countports", "Устанавливает количество портов, которые можно пропустить", x => Settings.Instance.General.TryCount = x, Settings.Instance.General.TryCount),
                new TypedCmdCmdArgument<int>("-daysUpdate", "Устанавливает период обновления сервера (в днях)", x => Settings.Instance.General.DaysUpdate = x, Settings.Instance.General.DaysUpdate),
                new TypedCmdCmdArgument<bool>("-recreate", "Указывает, необходимо ли пересоздать БД", x => Settings.Instance.General.IsRecreateDb = x, Settings.Instance.General.IsRecreateDb),
                new TypedCmdCmdArgument<string>("-dbname", "Устанавливает новое имя БД", x => Settings.Instance.General.DatabaseName = x, Settings.Instance.General.DatabaseName),
                new TypedCmdCmdArgument<string>("-tsvname", "Устанавливает новое имя файла с заведениями", x => Settings.Instance.General.TsvName = x, Settings.Instance.General.TsvName),
                new TypedCmdCmdArgument<string>("-settingsname", "Устанавливает новое имя для файла настроек", x => Settings.Instance.General.TripAdvisorUrl = x, Settings.Instance.General.TripAdvisorUrl)
            };
        }

        #endregion

        #region Вывод в консоль

        /// <summary>
        /// Устанавливает новую кодировку для input/output консоли.
        /// </summary>
        public static void SetEncoding(Encoding encoding) 
            => Console.OutputEncoding = Console.InputEncoding = encoding;

        /// <summary>
        /// Вывести исключение в консоль.
        /// </summary>
        /// <param name="exception">Вызывающее исключение</param>
        /// <param name="foregroundColor">Цвет текста</param>
        /// <param name="backgroundColor">Цвет фона</param>
        /// <param name="callerMethodName">Имя вызывающего метода (УКАЗЫВАЕТСЯ АВТОМАТИЧЕСКИ!)</param>
        /// <param name="lineNumber">Строка, вызвавшая метод (УКАЗЫВАЕТСЯ АВТОМАТИЧЕСКИ!)</param>
        public static void Write(Exception exception, 
            ConsoleColor? foregroundColor = null, 
            ConsoleColor? backgroundColor = null,
            [CallerMemberName] string callerMethodName = "",
            [CallerLineNumber] int lineNumber = -1)
            => Write($"<Исключение от {exception.Source}> {exception.Message}", foregroundColor ?? ConsoleColor.DarkRed, backgroundColor, callerMethodName, lineNumber);

        /// <summary>
        /// Вывести сообщение в консоль.
        /// </summary>
        /// <param name="message">Сообщение</param>
        /// <param name="foregroundColor">Цвет текста</param>
        /// <param name="backgroundColor">Цвет фона</param>
        /// <param name="callerMethodName">Имя вызывающего метода (УКАЗЫВАЕТСЯ АВТОМАТИЧЕСКИ!)</param>
        /// <param name="lineNumber">Строка, вызвавшая метод (УКАЗЫВАЕТСЯ АВТОМАТИЧЕСКИ!)</param>
        public static void Write(string message,
            ConsoleColor? foregroundColor = null, 
            ConsoleColor? backgroundColor = null, 
            [CallerMemberName] string callerMethodName = "",
            [CallerLineNumber] int lineNumber = -1)
        {
            SetSpecificConsoleColors(foregroundColor, backgroundColor);
            callerMethodName = foregroundColor != ConsoleColor.DarkRed ? " " : $" [{callerMethodName}, строка {lineNumber}] ";
            Console.WriteLine($"[{DateTime.Now:dd.MM HH:mm:ss}]{callerMethodName}{message}");
            SetSpecificConsoleColors(DefaultForegroundColor, DefaultBackgroundColor);
        }

        #endregion

        #region Установка цветов

        /// <summary>
        /// Устанавливает заданные цвета в консоли.
        /// </summary>
        /// <param name="foregroundColor">Цвет текста</param>
        /// <param name="backgroundColor">Цвет фоно</param>
        private static void SetSpecificConsoleColors(ConsoleColor? foregroundColor, ConsoleColor? backgroundColor)
        {
            if (foregroundColor != null)
                Console.ForegroundColor = (ConsoleColor) foregroundColor;
            if (backgroundColor != null)
                Console.BackgroundColor = (ConsoleColor) backgroundColor;
        }

        #endregion

        #region Аргументы командной строки

        /// <summary>
        /// Парсит аргументы командной строки.
        /// </summary>
        /// <param name="args">Аргументы командной строки.</param>
        public static void ParseArguments(string[] args)
        {
            var isError = false;
            for (var i = 0; i < args.Length; i++)
            {
                var cmd = _cmdArguments.FirstOrDefault(a => string.Equals(a.Command, args[i], StringComparison.CurrentCultureIgnoreCase));
                if (cmd == null)
                {
                    Write($"Неопознанный аргумент командной строки: {args[i]}!", ConsoleColor.DarkRed);
                    isError = true;
                    continue;
                }
                switch (cmd)
                {
                    case ITypedCmdArgument typedArg:
                        if (i >= args.Length)
                        {
                            Write($"Слишком мало аргументов для вызова команды {args[i]}!", ConsoleColor.DarkRed);
                            isError = true;
                            break;
                        }
                        typedArg.InvokeAction(args[i + 1]);
                        i++;
                        break;
                    case IUntypedCmdArgument typedArg:
                        typedArg.InvokeAction();
                        break;
                    default:
                        Write($"Неопознанный тип аргумента командной строки: {args[i]}!", ConsoleColor.DarkRed);
                        isError = true;
                        break;
                }
            }

            if (!isError) 
                return;
            
            Write("Правильный запуск сервера: ", ConsoleColor.DarkYellow);
            Write($"{AppDomain.CurrentDomain.FriendlyName} {string.Join(" ", _cmdArguments.Select(cmd => cmd.GetDefaultInvokation()))}", ConsoleColor.DarkYellow);
            Write("Описание аргументов описано при вызове -help", ConsoleColor.DarkYellow);
            Environment.Exit(Environment.ExitCode);
        }

        #endregion
    }
}