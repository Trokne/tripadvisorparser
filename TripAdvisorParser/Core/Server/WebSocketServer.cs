﻿using System;
using System.Collections.Generic;
using CsQuery.Engine.PseudoClassSelectors;
using WebSocketSharp;
using CsQuery.EquationParser.Implementation;
using CsQuery.ExtensionMethods;
using Newtonsoft.Json;
using TripAdvisorParser.Core.Data.Api.Commands;
using TripAdvisorParser.Core.Parser;
using TripAdvisorParser.Core.Server.CmdHandler;

namespace TripAdvisorParser.Core.Server
{
    public class WebSocketServer : IDisposable
    {
        #region Приватные переменные

        private readonly WebSocket _webSocket;
        private readonly Guid _socketIdentifier;
        private bool _isClosed;

        #endregion

        #region Публичные свойства

        /// <summary>
        /// Действия, выполняемые в случае успешного приёма сообщения.
        /// </summary>
        public Dictionary<string, Action<string>> ResponseActions { get; }

        #endregion

        #region Инициализация
        
        /// <summary>
        /// Инициализирует новый веб сокет.
        /// </summary>
        /// <param name="connectionUrl">Адрес подключения к сокету</param>
        public WebSocketServer(string connectionUrl)
        {
            _webSocket = new WebSocket(connectionUrl);
            _socketIdentifier = Guid.NewGuid();

            _webSocket.Log.Output = (data, s) => { }; // Отключение логирования websocketsharp
            _webSocket.OnMessage += ResponseMessage;
            _webSocket.OnError += ResponseError;
            _webSocket.OnOpen += ResponseOpen;
            _webSocket.OnClose += ResponseClose;
            
            ResponseActions = new Dictionary<string, Action<string>>();
        }
        
        /// <summary>
        /// Открывает соединение с сокетом.
        /// </summary>
        public void Connect()
            => _webSocket.Connect();
        
        /// <summary>
        /// Закрывает сокет и освобождает ресурсы, связанные с ним.
        /// </summary>
        public void Dispose()
        {
            _webSocket?.Close(CloseStatusCode.Away);
        }

        #endregion

        #region Обработка событий
        
        /// <summary>
        /// Отправляет заданное сообщение на сервер по веб-сокету.
        /// </summary>
        /// <param name="message">Сообщение</param>
        public void SendMessage(string message)
        {
            if (_isClosed)
            {
                throw new Exception("Попытка отправить сообщение на закрытый сокет.");
            }

            _webSocket.Send(message);
        }

        /// <summary>
        /// Принимает сообщение.
        /// </summary>
        private void ResponseMessage(object sender, MessageEventArgs e)
        {
            if (e.IsPing)
            {
                throw new Exception($"Наш сервер пингуют на сокет {_socketIdentifier.ToString()}. Не знаю что делать!");
            }

            if (!e.IsText)
            {
                throw new Exception($"На сокет {_socketIdentifier.ToString()} пришёл не текст! Не знаю что делать!");
            }

            InputOutput.Write($"На сервер по сокету {_socketIdentifier.ToString()} пришло сообщение.");
            var cmd = JsonConvert.DeserializeObject<Command>(e.Data);
            
            try
            {
                if (cmd.Action == "onopen")
                {
                    InputOutput.Write("Основной сервер подтвердил, что он жив. ", ConsoleColor.DarkGreen);
                }
                else
                {
                    ResponseActions[cmd.Id]?.Invoke(e.Data);
                }

            }
            catch (Exception)
            {
                InputOutput.Write("Сервер не знает, что делать с пришедшим сообщением!", ConsoleColor.DarkYellow);
                InputOutput.Write($"Сообщение: {e.Data}", ConsoleColor.DarkYellow);
            }
        }

        private void ResponseOpen(object sender, EventArgs e)
            => InputOutput.Write($"Открыто подключение к веб-сокету {_socketIdentifier.ToString()}.");

        private void ResponseClose(object sender, CloseEventArgs e)
        {
            _isClosed = true;
            var reason = !string.IsNullOrEmpty(e.Reason) ? $" по причине {e.Reason}" : "";
            InputOutput.Write($"Закрыто подключение к веб-сокету {_socketIdentifier.ToString()}{reason}.");
        }

        private void ResponseError(object sender, ErrorEventArgs e)
        {
            throw new Exception($"Веб-сокет {_socketIdentifier.ToString()} получил сообщение с ошибкой: {e}.");
        }

        #endregion

    }
}