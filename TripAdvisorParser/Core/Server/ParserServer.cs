﻿using System;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Runtime.Remoting;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SQLitePCL;
using TripAdvisorParser.Core.Data;
using TripAdvisorParser.Core.Data.Api;
using TripAdvisorParser.Core.Data.Api.Commands.Reviews;
using TripAdvisorParser.Core.Data.Database;
using TripAdvisorParser.Core.Helpers;
using TripAdvisorParser.Core.Parser;
using TripAdvisorParser.Core.Server.CmdHandler;
using Settings = TripAdvisorParser.Core.Data.Parameters.Settings;

namespace TripAdvisorParser.Core.Server
{
    public class ParserServer : Singleton<ParserServer>
    {
        #region Приватные переменные
        /// <summary>
        /// True, если сервер был запущен.
        /// </summary>
        private bool _isRunnedServer;

        /// <summary>
        /// Отвечает за обновление данных.
        /// </summary>
        private ServerUpdater _updater;
        /// <summary>
        /// Отвечает за поднятие сокета.
        /// </summary>
        private SocketHandler _socket;
        /// <summary>
        /// Отвечает за поднятие БД.
        /// </summary>
        private DatabaseHandler _database;

        #endregion

        #region Инициализация и запуск

        /// <summary>
        /// Запускает сервер.
        /// </summary>
        public async Task StartServer()
        {
            InputOutput.Write("Инициализация сервера парсера отзывов.");
            Console.CancelKeyPress += ShutdownServer;
            RunDatase();

            if (!Database.IsOpened)
            {
                return;
            }

            SourceDataParser.UpdateProfilesFromFile(Settings.Instance.General.TsvName);
            await Task.Run(() => SourceDataParser.UpdateProfilesFromServerAsync(1));
            
            _updater = new ServerUpdater(TimeSpan.FromDays(Settings.Instance.General.DaysUpdate));

            if (Settings.Instance.General.IsRecreateDb || !DatabaseHandler.IsExists<DbReview>())
            {
                _updater.Update(null, null);
            }
            else
            {
                _updater.CreateNewAlarm();
            }

            RunSocket();
        }

        /// <summary>
        /// Запускает сервер БД.
        /// </summary>
        private void RunDatase()
        {
            _database = new DatabaseHandler(AppDomain.CurrentDomain.BaseDirectory, Settings.Instance.General.DatabaseName);
            
            if (_database.TryOpen())
            {
                InputOutput.Write("Соединение с БД успешно установлено.", ConsoleColor.DarkGreen);
                return;
            }

            InputOutput.Write("Нет подключения к БД!", ConsoleColor.DarkRed);
            InputOutput.Write("Сервер не будет запущен!", ConsoleColor.DarkRed);

        }

        #endregion

        #region Публичные переменные и свойства

        /// <summary>
        /// База данных.
        /// </summary>
        public DatabaseHandler Database => _database;
        
        /// <summary>
        /// Время последнего обновления сервера.
        /// </summary>
        public DateTime LastUpdateTime => _updater.LastUpdateTime;
        /// <summary>
        /// Время следующего обновления сервера.
        /// </summary>
        public DateTime NextUpdateTime => _updater.NextUpdateTime;

        #endregion

        #region Поднятие сервера

        /// <summary>
        /// Открывает сокет для прослушивания.
        /// </summary>
        private void RunSocket()
        {
            if (!_database.IsOpened)
                return;
            
            _isRunnedServer = true;
            _socket = new SocketHandler();
            
            if (!_socket.TryRunSocket(Settings.Instance.General.StartPort, Settings.Instance.General.StartPort + Settings.Instance.General.TryCount))
            {
                _isRunnedServer = false;
                InputOutput.Write("Сервер не был запущен!", ConsoleColor.DarkRed);
                return;
            }
            
            while (_isRunnedServer)
            {
                _socket.Read();
            }
            _socket?.Close();
        }
        
        /// <summary>
        /// Выключает сервер
        /// </summary>
        private void ShutdownServer(object sender, ConsoleCancelEventArgs e)
        {
            InputOutput.Write("Останавливаем сервер...");
            _isRunnedServer = false;
            DatabaseHandler.Close();
            InputOutput.Write("Сервер остановлен.");
        }
        
        #endregion
    }
}