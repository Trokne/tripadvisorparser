﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Win32;
using TripAdvisorParser.Core.Data.Database;
using TripAdvisorParser.Core.Data.Parameters;
using TripAdvisorParser.Core.Helpers;
using TripAdvisorParser.Core.Parser;
using TripAdvisorParser.Core.Server.CmdHandler;

namespace TripAdvisorParser.Core.Server
{
    public class ServerUpdater
    {
        #region Приватные переменные

        /// <summary>
        /// Промежуток, с которым будут обновляться данные на сервере.
        /// </summary>
        private readonly TimeSpan _updateTime;
        /// <summary>
        /// Будильник
        /// </summary>
        private AlarmClock _alarm;

        #endregion

        #region Свойства
        
        /// <summary>
        /// Время последнего обновления сервера.
        /// </summary>
        public DateTime LastUpdateTime { get; private set; }
        public DateTime NextUpdateTime { get; private set; }

        #endregion

        #region Инициализация

        /// <summary>
        /// Инициализация обновителя отзывов на сервере.
        /// </summary>
        /// <param name="updateTime">Промежуток, через который будут обновляться данные на сервере.</param>
        public ServerUpdater(TimeSpan updateTime)
        {
            _updateTime = updateTime;
        }

        /// <summary>
        /// Создаёт новый будильник для обновления сервера.
        /// </summary>
        public void CreateNewAlarm()
        {
            LastUpdateTime = DateTime.Now;
            NextUpdateTime = DateTime.Now.Add(_updateTime);
            _alarm = new AlarmClock(NextUpdateTime);
            _alarm.Alarm += Update;
            PrintNextTimeUpdate();
        }

        #endregion

        #region Обновление сервера

        /// <summary>
        /// Обновляет информацию о отзывах на сервере.
        /// </summary>
        public async void Update(object sender, EventArgs eventArgs)
        {
            try
            {               
                CreateNewAlarm();
                InputOutput.Write("Начинаю полное обновление БД!");

                if (sender != null)
                {
                    SourceDataParser.UpdateProfilesFromFile(Settings.Instance.General.TsvName);
                    await Task.Run(() => SourceDataParser.UpdateProfilesFromServerAsync(1));
                }

                DatabaseHandler.StartTransaction();
                DatabaseHandler.ClearTable<DbReview>();
                foreach (var reviewsGroup in DatabaseHandler.GetTable<DbReviewsGroup>())
                {
                    ReviewsParser.TryParseProfileReviews(reviewsGroup);
                }
                InputOutput.Write("Данные на сервере полностью обновлены!", ConsoleColor.DarkGreen);
            }
            catch (Exception e)
            {
                InputOutput.Write(e);
            }
            finally
            {
                DatabaseHandler.EndTransaction();
               
            }          
        }

        /// <summary>
        /// Выводит следующее время обновления сервера.
        /// </summary>
        private void PrintNextTimeUpdate() 
            => InputOutput.Write($"Следующее полное обновление сервера произойдёт {NextUpdateTime}.");

        #endregion
    }
}