﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Configuration;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using SQLite;
using TripAdvisorParser.Core.Data.Database;
using TripAdvisorParser.Core.Helpers;
using TripAdvisorParser.Core.Server.CmdHandler;

namespace TripAdvisorParser.Core.Server
{
    public class DatabaseHandler
    {
        #region Приватные переменные

        /// <summary>
        /// Расположение БД
        /// </summary>
        private readonly string _path;
        /// <summary>
        /// Наименование БД
        /// </summary>
        private readonly  string _name;
        /// <summary>
        /// Полный путь до файла с БД.
        /// </summary>
        private readonly string _fullPath;
        /// <summary>
        /// БД в виде объекта.
        /// </summary>
        private static SQLiteConnection _db;
        
        #endregion

        #region Публичные свойства

        /// <summary>
        /// Проверяет, открыто ли подключение к БД
        /// </summary>
        public bool IsOpened { get; private set; }

        #endregion
        
        #region Инициализация и запуск/выключение
         
        public DatabaseHandler(string path, string name)
        {
            _path = path;
            _name = name;
            _fullPath = Path.Combine(path, name);
            InputOutput.Write($"Место расположения БД: {_fullPath}.");
        }

        /// <summary>
        /// Открывает подключение к БД.
        /// </summary>
        /// <returns>True, если подключение произошло корректно.</returns>
        public bool TryOpen()
        {
            try
            {
                if (!Directory.Exists(_path))
                    Directory.CreateDirectory(_path);
                
                _db = new SQLiteConnection(_fullPath);
                var tables = ReflectionHelper.GetInheritedTypes(typeof(IDatabaseEntity)).ToArray();
                _db.CreateTables(CreateFlags.None, tables);
                return IsOpened = true;
            }
            catch (Exception e)
            {
                InputOutput.Write(e);
                return IsOpened = false;
            }
            
            
        }

        /// <summary>
        /// Закрывает покдлючение к БД
        /// </summary>
        public static void Close()
        {
            _db.Close();
        }
        
        #endregion

        #region Работа с данными

        /// <summary>
        /// Получение таблицы для обработки запросов.
        /// </summary>
        /// <typeparam name="T">Таблица для запросов.</typeparam>
        /// <returns>Таблица, найденная в БД.</returns>
        public static TableQuery<T> GetTable<T>() where T : IDatabaseEntity, new()
            => _db.Table<T>();

        /// <summary>
        /// Вставляет новые значения в заданную таблицу.
        /// </summary>
        /// <param name="values">Значения для вставки</param>
        public static void InsertValues<T>(IEnumerable<T> values) where T : IDatabaseEntity, new()
            => _db.InsertAll(values);
        
        /// <summary>
        /// Вставляет или обновляет значения в заданную таблицу.
        /// </summary>
        /// <param name="values">Значения для вставки</param>
        public static void InsertOrReplaceValues<T>(IEnumerable<T> values) where T : IDatabaseEntity, new()
            => _db.InsertOrReplace(values);

        /// <summary>
        /// Вставляет или обновляет значения в заданную таблицу.
        /// </summary>
        /// <param name="value">Значение для вставки</param>
        public static void InsertOrReplaceValue<T>(object value) where T : IDatabaseEntity, new()
            => _db.InsertOrReplace(value);
        
        /// <summary>
        /// Получает все значения из таблицы, подходящие под заданное условие.
        /// </summary>
        /// <param name="func">Условие для получения значения.</param>
        /// <param name="isOnlyFirst">True, если необходимо остановить итерацию после получения первого значения.</param>
        /// <typeparam name="T">Таблица, из которой необходимо получить значение.</typeparam>
        /// <returns>Список значений, найденных в БД</returns>
        /// <remarks>
        /// Не кидайте в БД значения переменных, которые ещё не вычеслены!!!
        /// Плохой код: GetValues(g => g.Guid == values[0]).
        /// Хороший код: var value = values[0]; GetValues(g => g.Guid == value).
        /// </remarks>
        public static IEnumerable<T> GetValues<T>(Expression<Func<T, bool>> func, bool isOnlyFirst = false) where T : IDatabaseEntity, new()
        {           
            var query = GetTable<T>().Where(func);
            foreach (var obj in query)
            {
                yield return obj;
                
                if (isOnlyFirst)
                {
                    yield break;
                }    
            }
        }

        /// <summary>
        /// Удаляет значения, соответствующие заданному условию.
        /// </summary>
        /// <param name="func">Условие для получения значения.</param>
        /// <typeparam name="T">Таблица, из которой необходимо получить значение.</typeparam>
        public static void RemoveValues<T>(Expression<Func<T, bool>> func) where T : IDatabaseEntity, new()
        {
            var values = GetValues(func);
            
            if (values == null)
            {
                return;
            }

            foreach (var value in values)
            {
                _db.Delete<T>(value.Id);
            }
        }

        /// <summary>
        /// Получает первое значение из таблицы, соответствующее условию.
        /// </summary>
        /// <param name="func">Условие для получения значения</param>
        /// <typeparam name="T">Таблица, из которой необходимо получить значение.</typeparam>
        /// <returns>Первое значение, найденное в БД по заданному запросу</returns>
        /// <remarks>
        /// Не кидайте в БД значения переменных, которые ещё не вычеслены!!!
        /// Плохой код: GetFirstValue(g => g.Guid == values[0]).
        /// Хороший код: var value = values[0]; GetFirstValue(g => g.Guid == value).
        /// </remarks>
        public static T GetFirstValue<T>(Expression<Func<T, bool>> func) where T : IDatabaseEntity, new()
        {
            var values = GetValues(func, true);
            return values != null ? values.FirstOrDefault() : default;
        }

        /// <summary>
        /// Начать транзакцию.
        /// </summary>
        public static void StartTransaction()
            => _db.BeginTransaction();
        
        /// <summary>
        /// Закончить транзакцию.
        /// </summary>
        public static void EndTransaction()
            => _db.Commit();

        /// <summary>
        /// Проверяет, существует ли в БД значение, удовлетворяющее условию.
        /// </summary>
        /// <param name="func">Условие для получения значения</param>
        /// <typeparam name="T">Таблица, из которой необходимо получить значение.</typeparam>
        /// <returns>True, если значение существует.</returns>
        /// <remarks>
        /// Не кидайте в БД значения переменных, которые ещё не вычеслены!!!
        /// Плохой код: IsExists(g => g.Guid == values[0]).
        /// Хороший код: var value = values[0]; IsExists(g => g.Guid == value).
        /// </remarks>
        public static bool IsExists<T>(Expression<Func<T, bool>> func) where T : IDatabaseEntity, new()
            => _db.Table<T>().Count(func) != 0;
        
        /// <summary>
        /// Проверяет, не пуста ли таблица.
        /// </summary>
        /// <typeparam name="T">Таблица для проверки значений</typeparam>
        /// <returns>True, если таблица не пуста.</returns>
        public static bool IsExists<T>() where T : IDatabaseEntity, new()
            => _db.Table<T>().Count() != 0;
        
        /// <summary>
        /// Полностью очищает таблицу <see cref="T"/>.
        /// </summary>
        /// <typeparam name="T">Таблица для очистки</typeparam>
        public static void ClearTable<T>() where T : IDatabaseEntity, new()
            => _db.DeleteAll<T>();
        
        /// <summary>
        /// Вставляет новое значение в заданную таблицу.
        /// </summary>
        /// <param name="obj">Значение для вставки</param>
        /// <typeparam name="T">Таблица для вставки</typeparam>
        public static int InsertValue<T>(T obj) where T : IDatabaseEntity, new()
            => _db.Insert(obj, typeof(T));

        /// <summary>
        /// Удаляет заданное значение из БД.
        /// </summary>
        /// <param name="obj">Объект, который необходимо удалить.</param>
        /// <typeparam name="T">Таблица, из которой необходимо удалить объект</typeparam>
        public static void RemoveValue<T>(T obj) where T : IDatabaseEntity, new()
            => _db.Delete<T>(obj.Id);
        
        

        #endregion
    }
}