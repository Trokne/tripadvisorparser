﻿using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using TripAdvisorParser.Core.Data.Api;
using TripAdvisorParser.Core.Parser;
using TripAdvisorParser.Core.Server.CmdHandler;

namespace TripAdvisorParser.Core.Server
{
    /// <summary>
    /// Отвечает за работу с сокетом.
    /// </summary>
    public class SocketHandler
    {
        #region Приватные переменные и свойства

        /// <summary>
        /// Максимальная длина очереди ожидающих подключений.
        /// </summary>
        private const int QueueLength = int.MaxValue;
        
        /// <summary>
        /// Прослушиваемый сокет
        /// </summary>
        private Socket _socket;

        #endregion

        #region Запуск и выключение сокета

        /// <summary>
        /// Пробует запустить сокет на заданном порте. В случае неудачи прибавляет к порту 1.
        /// </summary>
        /// <param name="startPort">Порт, с которого начинается запуск сервера.</param>
        /// <param name="endPort">Порт, на котором заканчивается запуск сервера в случае неудачи.</param>
        /// <returns>True, если сокет был запущен.</returns>
        public bool TryRunSocket(int startPort, int endPort)
        {
            var ipPoint = new IPEndPoint(IPAddress.Any, startPort);
            _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                _socket.Bind(ipPoint);
                _socket.Listen(QueueLength);
                InputOutput.Write($"Сервер запущен на порте {ipPoint.Port}. Ожидание подключений...", ConsoleColor.DarkGreen);
                return true;
            }
            catch (Exception ex)
            {
                if (startPort <= endPort)
                {
                    return TryRunSocket(startPort + 1, endPort);
                }

                InputOutput.Write(ex);
                return false;
            }

        }

        /// <summary>
        /// Выключает сокет.
        /// </summary>
        public void Close()
        {
            _socket?.Shutdown(SocketShutdown.Both);
            _socket?.Close();

        }

        #endregion

        #region Чтение сообщений

        /// <summary>
        /// Мониторит приходящие сообщения.
        /// </summary>
        public void Read()
        {
            try
            {
                using (var handler = _socket.Accept())
                {
                    InputOutput.Write("Сервер принял сообщение!");
                    var message = new StringBuilder();
                    var data = new byte[256];
                    do
                    {
                        var bytes = handler.Receive(data);
                        message.Append(Encoding.Unicode.GetString(data, 0, bytes));
                    }
                    while (handler.Available > 0);
                    var result = CommandParser.InterpretierClientCommand(message.ToString());
                    handler.Send(Encoding.Unicode.GetBytes(result));
                    InputOutput.Write("Сервер отправил ответ!");
                }
            }
            catch (Exception e)
            {
                InputOutput.Write(e);
            }

        }

        #endregion
        
    }
}