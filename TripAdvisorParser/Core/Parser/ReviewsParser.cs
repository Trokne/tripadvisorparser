﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using CsQuery;
using Newtonsoft.Json;
using TripAdvisorParser.Core.Data;
using TripAdvisorParser.Core.Data.Database;
using TripAdvisorParser.Core.Data.Parameters;
using TripAdvisorParser.Core.Data.Reviews;
using TripAdvisorParser.Core.Server;
using TripAdvisorParser.Core.Server.CmdHandler;

namespace TripAdvisorParser.Core.Parser
{
    public static class ReviewsParser
    {
        #region Общение с БД

        /// <summary>
        /// Получает группу отзывов у заданного профиля заведения.
        /// </summary>
        /// <param name="profileGuid">GUID профиля заведения.</param>
        /// <param name="reviewsGroup">Группа отзывов заданного профиля.</param>
        /// <returns>True, если группу удалось получить.</returns>
        private static bool TryGetProfileReviewGroup(string profileGuid, out DbReviewsGroup reviewsGroup)
        {
            reviewsGroup = null;
            var profile = DatabaseHandler.GetFirstValue<DbProfile>(p => p.Id == profileGuid);
            
            if (profile?.ReviewsGroupId == null)
            {
                InputOutput.Write($"Профиль заведения {profileGuid} отсутствует в БД или у него нет ссылки на tripadvisor!", ConsoleColor.DarkRed);
                return false;
            }

            reviewsGroup = DatabaseHandler.GetFirstValue<DbReviewsGroup>(g => g.Id == profile.ReviewsGroupId);

            if (reviewsGroup != null)
            {
                return true;
            }

            InputOutput.Write($"Не найдена страница с отзывами заведения {profileGuid}", ConsoleColor.DarkRed);
            return false;

        }
        
        /// <summary>
        /// Получает id у ReviewsGroup заданной группы заведений.
        /// </summary>
        /// <param name="groupGuid">GUID группы заведений.</param>
        /// <param name="reviewGroupIndicies">Место, куда будут записаны id.</param>
        /// <returns>True, если список id групп отзывов удалось получить.</returns>
        private static bool TryGetReviewGroupIndicies(string groupGuid, out HashSet<string> reviewGroupIndicies)
        {
            reviewGroupIndicies = null;
            
            if (!ParserServer.Instance.Database?.IsOpened ?? false)
            {
                InputOutput.Write("Подключение к БД отсутствует!", ConsoleColor.DarkRed);
                return false;
            }
            
            if (string.IsNullOrWhiteSpace(groupGuid)
                || !DatabaseHandler.IsExists<DbGroup>(g => g.Id == groupGuid))
            {
                InputOutput.Write($"В БД отсутствует группа с GUID {groupGuid}!", ConsoleColor.DarkRed);
                return false;
            }
            
            var profileTable = DatabaseHandler.GetTable<DbProfile>();
            reviewGroupIndicies = new HashSet<string>();
            
            foreach (var profile in profileTable)
            {
                if (profile.GroupId == groupGuid
                    && profile.ReviewsGroupId != null)
                {
                    reviewGroupIndicies.Add(profile.ReviewsGroupId);
                }
            }

            if (reviewGroupIndicies.Count != 0)
            {
                return true;
            }

            InputOutput.Write($"У группы {groupGuid} отсутствуют ссылки на tripadvisor!", ConsoleColor.DarkRed);
            return false;

        }
        
        /// <summary>
        /// Удаляет отзывы с заданным ReviewGroupId.
        /// </summary>
        /// <param name="reviewGroupId">Группа отзывов из БД.</param>
        private static void RemoveReviews(string reviewGroupId)
            => DatabaseHandler.RemoveValues<DbReview>(r => r.ReviewGroupId == reviewGroupId);
        
        #endregion

        #region Запросы парсинга

        /// <summary>
        /// Пробует занести в БД отзывы о заведениях, принадлежащих заданной группе.
        /// </summary>
        /// <param name="groupGuid">GUID группы заведений</param>
        /// <param name="reviewGroupsIndicies">Полученные ID групп отзывов после парсинга</param>
        /// <param name="isNeedRecreating">True, если необходимо по новой собрать отзывы.</param>
        /// <returns></returns>
        public static bool TryParseGroupReviews(string groupGuid, out HashSet<string> reviewGroupsIndicies, bool isNeedRecreating = false)
        {
            if (!TryGetReviewGroupIndicies(groupGuid, out var reviewGroupIndicies))
            {
                reviewGroupsIndicies = null;
                return false;
            }

            reviewGroupsIndicies = new HashSet<string>();
            foreach (var reviewGroup in DatabaseHandler.GetTable<DbReviewsGroup>())
            {
                var reviewGropId = reviewGroup.Id;

                if (!reviewGroupIndicies.Contains(reviewGropId))
                    continue;

                reviewGroupsIndicies.Add(reviewGropId);
                
                if (!isNeedRecreating)
                {
                    continue;
                }
                
                RemoveReviews(reviewGroup.Id);
                
                if (!TryParseRestaurant(reviewGroup))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Пробует занести в БД отзывы о заданном заведении.
        /// </summary>
        /// <param name="profileGuid">GUID профиля заведения</param>
        /// <param name="reviewGroupId">Полученное ID группы отзывов после парсинга</param>
        /// <param name="isNeedRecreating">True, если необходимо по новой собрать отзывы.</param>
        public static bool TryParseProfileReviews(string profileGuid, out string reviewGroupId, bool isNeedRecreating = false)
        {
            if (!TryGetProfileReviewGroup(profileGuid, out var reviewGroup))
            {
                reviewGroupId = null;
                return false;
            }

            reviewGroupId = reviewGroup.Id;
            
            if (!isNeedRecreating)
            {
                return true;
            }

            RemoveReviews(reviewGroup.Id);
            return TryParseRestaurant(reviewGroup);
        }

        /// <summary>
        /// Пробует занести в БД отзывы по заданной группе отзывов.
        /// </summary>
        /// <param name="reviewGroup">Группа отзывов о заведении</param>
        public static bool TryParseProfileReviews(DbReviewsGroup reviewGroup)
        {
            RemoveReviews(reviewGroup.Id);
            return TryParseRestaurant(reviewGroup);
        }

        #endregion

        #region Парсинг

        /// <summary>
        /// Парсит страницу с отзывами.
        /// </summary>
        private static bool TryParseRestaurant(DbReviewsGroup reviewsGroup)
        {
            InputOutput.Write($"Начало считывания отзывов со страницы {reviewsGroup.Url}");
            if (!IsCorrectUrl(reviewsGroup.Url))
            {
                InputOutput.Write("На вход парсеру подана некорректная страница ресторана!", ConsoleColor.DarkRed);
                return false;
            }

            var count = GetReviewsCount(reviewsGroup.Url);
            
            if (count == -1)
            {
                InputOutput.Write("На странице с отзывами не найдено их количество!", ConsoleColor.DarkRed);
                return false;
            }
            
            InputOutput.Write($"Количество отзывов для считывания: {count}");
            
            var pages = count / 10 * 10;
            
            InputOutput.Write($"Считывается страница с отзывами: 1/{pages / 10 + 1}");
            var reviews = GetReviews(reviewsGroup.Url).ToList();
            
            for (var curPage = 10; curPage <= pages; curPage += 10)
            {
                InputOutput.Write($"Считывается страница с отзывами: {curPage / 10 + 1}/{pages / 10 + 1}");
                reviews.AddRange(GetReviews(GetPage(reviewsGroup.Url, curPage)));
            }

            var dbReviews = reviews.Select(review => review.ToDbReview(reviewsGroup.Id));
            DatabaseHandler.InsertValues(dbReviews);
            
            InputOutput.Write($"Отзывы ({reviews.Count} шт.) успешно считаны!", ConsoleColor.DarkGreen);
            
            return true;
        }

        /// <summary>
        /// Получает общее количество отзывов с первой страницы.
        /// </summary>
        /// <param name="pageUrl">Первая страница с отзывами</param>
        /// <returns>Общее количество отзывов.</returns>
        private static int GetReviewsCount(string pageUrl)
        {
            var cQuery = CQ.CreateFromUrl(pageUrl);
            foreach (var domObject in cQuery.Find("span.reviews_header_count.block_title"))
            {
               return Convert.ToInt32(domObject.ChildNodes[0].ToString().Replace(")", "").Replace("(", ""));
            }
            return -1;
        }
        
        /// <summary>
        /// Получает список отзывов с одной страницы.
        /// </summary>
        /// <param name="pageUrl">Страница с отзывами</param>
        /// <returns></returns>
        private static IEnumerable<Review> GetReviews(string pageUrl)
        {
            var cQuery = CQ.CreateFromUrl(pageUrl);
            foreach (var domObject in cQuery.Find("span.noQuotes"))
            {
                var partOfUrl = domObject.ParentNode["href"];
                if (!string.IsNullOrWhiteSpace(partOfUrl))
                    yield return ParseReview(Settings.Instance.General.TripAdvisorUrl + partOfUrl);
            }
        }

        /// <summary>
        /// Парсит ссылку на отзыв.
        /// </summary>
        private static Review ParseReview(string reviewUrl)
        {
            if (!IsCorrectUrl(reviewUrl))
            {
                InputOutput.Write("На вход парсеру подана некорректная ссылка на отзыв!", ConsoleColor.DarkRed);
                return null;
            }
            
            var cQuery = CQ.CreateFromUrl(reviewUrl);
            foreach (var domObject in cQuery.Find("script[type=\"application/ld+json\"]"))
            {
                var reviewDom = domObject.FirstChild.ToString();
                
                if (string.IsNullOrWhiteSpace(reviewDom) || !reviewDom.Contains("reviewBody")) 
                    continue;
                
                var review = JsonConvert.DeserializeObject<Review>(reviewDom);                
                return review;
            }

            return null;

        }
        

        #endregion

        #region Проверки

        /// <summary>
        /// Проверяет URL на корректность.
        /// </summary>
        /// <param name="url">Ссылка для проверки.</param>
        private static bool IsCorrectUrl(string url)
            => Uri.IsWellFormedUriString(url, UriKind.Absolute);
        
        /// <summary>
        /// Получает страницу с отзывами.
        /// </summary>
        /// <param name="url">Изначальная ссылка на заведение.</param>
        /// <param name="page">Страница.</param>
        /// <returns>Ссылка на страницу с отзывами.</returns>
        private static string GetPage(string url, int page)
        {
            var inputIndex = url.IndexOf("Reviews", StringComparison.CurrentCulture);
            return url.Insert(inputIndex, $"or{page}-");
        }

        #endregion
    }
}