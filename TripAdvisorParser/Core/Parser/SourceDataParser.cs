﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Security.AccessControl;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using TripAdvisorParser.Core.Data.Api.Commands;
using TripAdvisorParser.Core.Data.Api.Commands.Url;
using TripAdvisorParser.Core.Data.Api.Commands.Url.Request;
using TripAdvisorParser.Core.Data.Api.Commands.Url.Response;
using TripAdvisorParser.Core.Data.Database;
using TripAdvisorParser.Core.Data.Parameters;
using TripAdvisorParser.Core.Helpers;
using TripAdvisorParser.Core.Server;
using TripAdvisorParser.Core.Server.CmdHandler;

namespace TripAdvisorParser.Core.Parser
{
    /// <summary>
    /// Отвечает за парсинг данных о заведениях.
    /// </summary>
    public static class SourceDataParser
    {
        private static bool _isNeedToUpdate;
        private static bool _isResponsedMessageFromServer;
        
        #region Загрузка исходных данных

        /// <summary>
        /// Обновляет информацию о заведениях в БД из tsv файла.
        /// </summary>
        /// <param name="path">Путь к файлу с исходными данными (обязательно табулированный TSV).</param>
        public static void UpdateProfilesFromFile(string path)
        {
            try
            {
                InputOutput.Write($"Начало считывания списка заведений из {Settings.Instance.General.TsvName}");
                DatabaseHandler.StartTransaction();
                
                foreach (var line in File.ReadAllLines(path))
                {
                    var values = line.Split('\t');
                    
                    var groupGuid = values[0];
                    var profileGuid = values[1];
                    var url = values[2];
                    var name = values[3];
                    var street = values[4];
                    
                    CreateOrUpdateGroup(groupGuid, name);
                    var reviewsGroup = GetOrCreateReviewGroup(url);
                    CreateOrUpdateProfile(groupGuid, profileGuid, street, reviewsGroup?.Id);
                }
                
                InputOutput.Write($"Список заведений из {Settings.Instance.General.TsvName} успешно считан.");
            }
            catch (Exception e)
            {
                InputOutput.Write(e);
                
            }
            finally
            {
                DatabaseHandler.EndTransaction();
            }
        }

        /// <summary>
        /// Запрашивает информацию о списке заведений с основного сервера.
        /// </summary>
        public static async Task UpdateProfilesFromServerAsync(uint page)
        {
            _isResponsedMessageFromServer = false;
            _isNeedToUpdate = false;
            
            try
            {
                InputOutput.Write("Начало считывания заведений с основного сервера.");
                
                using (var ws = new WebSocketServer(Settings.Instance.WebSocket.GetCombined()))
                {
                    ws.Connect();
                    
                    var options = new RequestLinksOptions(page, Settings.Instance.WebSocket.MaxPageSize);
                    var request = new RequestCommand<RequestLinksOptions>(options, "api.grouporg.tripadvisor.list");
                    var json = JsonConvert.SerializeObject(request);
                    ws.ResponseActions.Add(request.Id, UpdateProfilesFromServer);
                    ws.SendMessage(json);

                    await TaskWaiter.WaitUntil(() => _isResponsedMessageFromServer);
                    
                    if (_isNeedToUpdate)
                    {
                        await UpdateProfilesFromServerAsync(page + 1);
                        return;
                    }
                    
                    InputOutput.Write("Отзывы с сервера полностью считаны.");
                }
            }
            catch (Exception)
            {
                InputOutput.Write("Список заведений не был считан с сервера.");
            }
        }

        /// <summary>
        /// Обновляет информацию на сервере в соответствиии с пришедшей информацией.
        /// </summary>
        /// <param name="data">Пришедшая информация</param>
        /// <exception cref="Exception">Веб-сокет получил сообщение с ошибочным статусом (!= 1000)</exception>
        private static void UpdateProfilesFromServer(string data)
        {
            try
            {
                var generalCommand = JsonConvert.DeserializeObject<StatusCommand>(data);
                if (generalCommand.CmdStatus != Command.Status.Success)
                {
                    throw new Exception("Веб сокет получил сообщение с ошибочным статусом: " +
                                        $"{generalCommand.CmdStatus.ToString()} (№{generalCommand.CmdStatus})");
                }

                var trueCommand = JsonConvert.DeserializeObject<ResponseCommand<List<ResponseLinksGroup>>>(data);
                var groups = trueCommand.Result;

                _isNeedToUpdate = groups.Count == Settings.Instance.WebSocket.MaxPageSize;
                InputOutput.Write($"С сервера пришли группы в размере {groups.Count} шт.");
                
                foreach (var group in groups)
                {
                    CreateOrUpdateGroup(group.GroupId, group.Name);
                    
                    foreach (var profile in group.Profiles)
                    {
                        var reviewsGroup = GetOrCreateReviewGroup(profile.Url);
                        CreateOrUpdateProfile(group.GroupId, profile.Id, profile.Addreess, reviewsGroup?.Id);
                    }
                }

            }
            catch (Exception e)
            {
                InputOutput.Write(e);
                _isNeedToUpdate = false;
            }
            finally 
            {
                _isResponsedMessageFromServer = true;
            }
        }

        #endregion

        #region Работа с БД

        /// <summary>
        /// Создаёт или получает группу.
        /// </summary>
        /// <param name="groupGuid">GUID группы</param>
        /// <param name="name">Имя группы</param>
        /// <returns>Группа</returns>
        private static void CreateOrUpdateGroup(string groupGuid, string name)
        {
            var existsGroup = DatabaseHandler.GetFirstValue<DbGroup>(g => g.Id == groupGuid);

            if (existsGroup != null)
            {
                return;
            }

            existsGroup = new DbGroup { Id = groupGuid, Name = name };
            DatabaseHandler.InsertValue(existsGroup);
        }

        /// <summary>
        /// Создаёт или получает группу отзывов.
        /// </summary>
        /// <param name="url">Ссылка на отзывы о заведении.</param>
        /// <returns></returns>
        private static DbReviewsGroup GetOrCreateReviewGroup(string url)
        {
            if (string.IsNullOrWhiteSpace(url)
                || UrlHelper.IsEqualsUrls(Settings.Instance.General.TripAdvisorUrl, url))
            {
                return null;
            }

            var existsReviewGroup = DatabaseHandler.GetFirstValue<DbReviewsGroup>(r => r.Url == url);
            
            if (existsReviewGroup != null)
                return existsReviewGroup;
            
            existsReviewGroup = new DbReviewsGroup
            {
                Url = url,
                Id = Guid.NewGuid().ToString()
            };
            
            DatabaseHandler.InsertValue(existsReviewGroup);
            return existsReviewGroup;
        }
        
        /// <summary>
        /// Создаёт или получает профиль.
        /// </summary>
        /// <param name="groupId">GUID группы</param>
        /// <param name="profileId">GUID заведения</param>
        /// <param name="address">Адрес заведения</param>
        /// <param name="reviewId">ID группы отзывов</param>
        /// <returns>Группа</returns>
        private static void CreateOrUpdateProfile(string groupId, string profileId, string address, string reviewId)
        {
            var profile = DatabaseHandler.GetFirstValue<DbProfile>(p => p.Id == profileId) ?? new DbProfile(profileId);
            profile.Address = address;
            profile.GroupId = groupId;
            
            if (reviewId != null)
            {
                profile.ReviewsGroupId = reviewId;
            }
            
            DatabaseHandler.InsertOrReplaceValue<DbProfile>(profile);
        }
        
        #endregion


    }
}