﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using Newtonsoft.Json;
using TripAdvisorParser.Core.Data.Api;
using TripAdvisorParser.Core.Data.Api.Commands;
using TripAdvisorParser.Core.Data.Api.Commands.Reviews.Response;
using TripAdvisorParser.Core.Helpers;
using TripAdvisorParser.Core.Server;
using TripAdvisorParser.Core.Server.CmdHandler;
using ICommand = System.Windows.Input.ICommand;

namespace TripAdvisorParser.Core.Parser
{
    public static class CommandParser
    {
        #region Приватные переменные и свойства

        private static IEnumerable<Type> _commandTypes;
        /// <summary>
        /// Типы комманд, доступных для сервера.
        /// </summary>
        private static IEnumerable<Type> CommandTypes
        {
            get => _commandTypes ?? (_commandTypes = ReflectionHelper.GetInheritedTypes(typeof(Command)));
            set => _commandTypes = value;
        }

        #endregion

        #region Интерпретация команд

        /// <summary>
        /// Пробует интерпретировать комманду, пришедшую с клиента MenuAR.
        /// </summary>
        /// <param name="jsonCommand">Команда в текстовом виде</param>
        /// <returns>Результат выполнения команды</returns>
        public static string InterpretierClientCommand(string jsonCommand)
        {
            try
            {
                var result = Interpretier<string>(jsonCommand);
                return result;
            }
            catch (Exception e)
            {
                InputOutput.Write(e);
                var errorResponse = new ResponseCommand<ResponseReviewsOptions>(Command.Status.RequestError);
                var result =  JsonConvert.SerializeObject(errorResponse);
                return result;
            }
        }
        
        /// <summary>
        /// Пробует интерпретировать комманду без необходимости вывода результата.
        /// </summary>
        /// <param name="jsonCommand">Команда в текстовом виде</param>
        public static void InterpretierVoidCommand(string jsonCommand)
        {
            try
            {
                Interpretier<string>(jsonCommand);
            }
            catch (Exception e)
            {
                InputOutput.Write(e);
            }
        }

                
        /// <summary>
        /// Интерпретирует команду, которая пришла на сервер.
        /// </summary>
        /// <param name="command">Команда, пришедшая на сервер.</param>
        /// <typeparam name="TResult"></typeparam>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        private static TResult Interpretier<TResult>(string command) where TResult : class 
        {
            if (string.IsNullOrWhiteSpace(command))
                throw new Exception("На сервер пришла пустая команда");
            Command cmdGenericInfo = null;
            
            cmdGenericInfo = JsonConvert.DeserializeObject<Command>(command);
            var cmdType = CommandTypes.FirstOrDefault(type => ReflectionHelper.GetDescription(type) == cmdGenericInfo.Action);

            if (cmdType == null)
                throw new Exception("Запрашиваемая в запросе команда не найдена!");

            var resultMethod = cmdType.GetMethod("GetResult");
            var result = resultMethod?.Invoke(null, new object[] {command});
            return result as TResult;
        }

        #endregion
       
    }
}