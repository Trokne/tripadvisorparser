﻿using Newtonsoft.Json;


namespace TripAdvisorParser.Core.Data.Reviews
{
    public class Rating
    {
        [JsonProperty("@type")]
        public string Type { get; set; }
        [JsonProperty("ratingValue")]
        public float RatingValue { get; set; }
    }
}