﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using TripAdvisorParser.Core.Data.Api.Commands;
using TripAdvisorParser.Core.Data.Database;
using TripAdvisorParser.Core.Helpers;
using TripAdvisorParser.Core.Server.CmdHandler;

namespace TripAdvisorParser.Core.Data.Reviews
{
    public class Review
    {
        private string _name;
        private string _text;

        [JsonProperty("id")]
        public string Id { get; set; }
        
        [JsonProperty("reviewGroupId")]
        public string ReviewGroupId { get; set; }
        
        [JsonProperty("name")]
        public string Name
        {
            get => _name;
            set
            {
                var name = Regex.Unescape(value);;
                _name = GetFormattedText(name);
            }
        }

        [JsonProperty("reviewBody")]
        public string Text
        {
            get => _text;
            set
            {
                var text = Regex.Unescape(value);
                _text = GetFormattedText(text);
            }
        }

        [JsonProperty("image")]
        public string Image { get; set; }
        [JsonProperty("datePublished")]
        public string Date { get; set; }
        [JsonProperty("reviewRating")]
        public Rating ReviewRating { get; set; }

        /// <summary>
        /// Фильтрует список отзывов.
        /// </summary>
        /// <param name="reviews">Список отзывов</param>
        /// <param name="dateSort">Какую сортировку по дате необходимо применить</param>
        /// <param name="pageNum">Номер страницы с отзывами</param>
        /// <param name="pageSize">Рзамер страницы с отзывами</param>
        public static void Filter(ref List<Review> reviews, Command.SortType dateSort, int pageNum, int pageSize)
        {
            if (pageNum <= 0 || pageSize <= 0 || reviews?.Count == 0)
            {
                InputOutput.Write($"Неудачная попытка фильтрации отзывов ({reviews?.Count ?? 0} шт.) " +
                                  $"со страницы {pageNum} в размере {pageSize} шт.", ConsoleColor.DarkRed);
                reviews = new List<Review>();
                return;
            }

            SortByDate(ref reviews, dateSort);

            try
            {
                var startIndex = (pageSize * pageNum - pageSize).Clamp(0, int.MaxValue);
                if (startIndex > reviews.Count)
                {
                    throw new Exception("Попытка взять страницу с неправильным диапазоном.");
                }
                var count = startIndex + pageSize;
                if (count >= reviews.Count)
                    count = reviews.Count - startIndex;
                
                reviews = reviews.GetRange(startIndex, count);
            }
            catch (Exception e)
            {
                InputOutput.Write(e);
                reviews = new List<Review>();
            }

        }

        /// <summary>
        /// Сортирует отзывы по дате.
        /// </summary>
        /// <param name="reviews">Отзывы</param>
        /// <param name="dateSort">Вид сортировки</param>
        private static void SortByDate(ref List<Review> reviews, Command.SortType dateSort)
        {
            try
            {
                reviews = dateSort == Command.SortType.Ascending 
                    ? reviews.OrderBy(r => DateTime.Parse(r.Date)).ToList() 
                    : reviews.OrderByDescending(r => DateTime.Parse(r.Date)).ToList();
            }
            catch (Exception e)
            {
                InputOutput.Write("Сортировка отзывов по дате не удалась. Возможно, формат даты на сайте был изменен.", ConsoleColor.DarkRed);
                InputOutput.Write(e);
                InputOutput.Write("Будет произведена сортировка по строкам.");
                
                reviews = dateSort == Command.SortType.Ascending 
                    ? reviews.OrderBy(r => r.Date).ToList() 
                    : reviews.OrderByDescending(r => r.Date).ToList();
            }
        }

        public DbReview ToDbReview(string reviewGroupId)
        {
            var review = new DbReview
            {
                Date = Date,
                Image = Image,
                Name = Name,
                Rating = ReviewRating.RatingValue,
                ReviewGroupId = reviewGroupId,
                Text = Text,
                Id = Guid.NewGuid().ToString()
            };
            return review;
        }
        
        /// <summary>
        /// Возвращает текст, не содержащий некорректные символы.
        /// </summary>
        /// <param name="text">Исодный текст для правки</param>
        /// <returns>Текст, не содержащий некорректные символы</returns>
        private static string GetFormattedText(string text)
        {
            text = text
                .Replace("&quot;", "\"")
                .Replace("\t", "")
                .Replace("  ", "");
                
            text = Regex.Replace(text, @"\p{Cs}", "");
            text = text.Trim();
            
            return text;
        }
    }
}