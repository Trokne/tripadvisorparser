﻿using Newtonsoft.Json;

namespace TripAdvisorParser.Core.Data.Parameters
{
    public class WebSocketSettings
    {
        #region Свойства

        [JsonProperty("Протокол")]
        public string Protocol { get; set; } = "ws";

        [JsonProperty("Адрес")]
        public string Address { get; set; } = "192.168.0.220";

        [JsonProperty("Порт")]
        public uint Port { get; set; } = 1000;

        [JsonProperty("Логин")]
        public string Login { get; set; } = "anonymous";

        [JsonProperty("Токен")]
        public string Token { get; set; } = "null";

        [JsonProperty("ID устройства")]
        public string DeviceId { get; set; } = "null";

        [JsonProperty("Язык")]
        public string LanguageCode { get; set; } = "en";

        [JsonProperty("Максимальный размер страниицы для запросов")]
        public uint MaxPageSize { get; set; } = 50;

        #endregion

        #region Методы по объединению

        /// <summary>
        /// Получает собранную строку подключения к основному серверу.
        /// </summary>
        /// <returns>Строка подключения к основному серверу</returns>
        public string GetCombined()
        {
            var connectionUrl = $@"{Protocol}://{Address}:{Port}/" +
                                @"Client?" +
                                $@"login={Login}" +
                                $@"&token={Token}" +
                                $@"&deviceid={DeviceId}" +
                                $@"&lang={LanguageCode}";
            return connectionUrl;
        }

        #endregion
        
    }
}