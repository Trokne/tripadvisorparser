﻿using System;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using TripAdvisorParser.Core.Helpers;
using TripAdvisorParser.Core.Server.CmdHandler;

namespace TripAdvisorParser.Core.Data.Parameters
{
    /// <summary>
    /// Описывает настройки программы.
    /// </summary>
    public class Settings : Singleton<Settings>
    {
        #region Сохранение и загрузка

        /// <summary>
        /// Загружает данные из файла настроек, если такой есть. Если нет - создаёт и заполняет его.
        /// </summary>
        /// <returns>True, если загрузка/создание прошли успешно.</returns>
        public bool TryLoadFile()
        {           
            try
            { 
                if (!File.Exists(General.ParamsName))
                {
                    InputOutput.Write("Файл настроек не найден, будет создан новый.");
                    var isSavedSettings = TrySaveSettings();
                    return isSavedSettings;
                }
                
                InputOutput.Write("Файл настроек найден. Десериализация начата.");
                var readedJson = File.ReadAllText(General.ParamsName);
                var deserializedSettings = JsonConvert.DeserializeObject<Settings>(readedJson);
                Instance = deserializedSettings ?? this;
                
                return deserializedSettings != null;
            }
            catch (Exception e)
            {
                InputOutput.Write(e);
                return false;
            }
        }

        /// <summary>
        /// Сохраняет настройки в файл.
        /// </summary>
        /// <returns>True, если настройки были сохранены.</returns>
        public bool TrySaveSettings()
        {
            try
            {
                var jsonToWrite = JsonConvert.SerializeObject(this, Formatting.Indented);
                File.WriteAllText(General.ParamsName, jsonToWrite, Encoding.UTF8);
                return true;
            }
            catch (Exception e)
            {
                InputOutput.Write(e);
                return false;
            }

        }

        #endregion

        #region Параметры

        /// <summary>
        /// Настройки для подключения к серверу MenuAR
        /// </summary>
        [JsonProperty("Основные настройки")]
        public GeneralSettings General { get; set; } = new GeneralSettings();
        
        /// <summary>
        /// Настройки для подключения к серверу MenuAR
        /// </summary>
        [JsonProperty("Настройки для подключения к основному серверу MenuAR")]
        public WebSocketSettings WebSocket { get; set; } = new WebSocketSettings();
        

        #endregion
    }
}