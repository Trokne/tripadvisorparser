﻿using Newtonsoft.Json;

namespace TripAdvisorParser.Core.Data.Parameters
{
    public class GeneralSettings
    {      
        #region Параметры
        
        /// <summary>
        /// Имя файла с параметрами.
        /// </summary>
        [JsonIgnore]
        public string ParamsName { get; set; } = "settings.json";

        /// <summary>
        /// Имя базы данных.
        /// </summary>
        [JsonProperty("Имя БД")]
        public string DatabaseName { get; set; } = "Parser.db";

        /// <summary>
        /// Имя файла со списком заведений.
        /// </summary>
        [JsonProperty("Имя файла с заведениями")]
        public string TsvName { get; set; } = "restaurants.tsv";

        /// <summary>
        /// Порт, с которого необходимо начинать запуск сервера
        /// </summary>
        [JsonProperty("Порт")]
        public int StartPort { get; set; } = 1000;

        /// <summary>
        /// Количество попыток подключений к новым портам.
        /// </summary>
        [JsonProperty("Количество следующих портов для запуска")]
        public int TryCount { get; set; } = 0;

        /// <summary>
        /// Промежуток (в днях), с которым сервер обновляет данные об отзывах.
        /// </summary>
        [JsonProperty("Интервал обновления сервера (в днях)")]
        public int DaysUpdate { get; set; } = 3;

        /// <summary>
        /// True, если необходимо пересоздать БД.
        /// </summary>
        [JsonProperty("Пересоздание БД")]
        public bool IsRecreateDb { get; set; } = false;

        /// <summary>
        /// Ссылка на сайт TripAdvisor.
        /// </summary>
        [JsonProperty("Ссылка на TripAdvisor")]
        public string TripAdvisorUrl { get; set; } = @"https://www.tripadvisor.ru";

        #endregion

    }
}