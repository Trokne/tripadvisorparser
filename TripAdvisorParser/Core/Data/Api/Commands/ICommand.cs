﻿namespace TripAdvisorParser.Core.Data.Api.Commands
{
    public interface ICommand
    {        
        /// <summary>
        /// ID запроса
        /// </summary>
        string Id { get; set; }
        /// <summary>
        /// Действие, которое необходимо выполнить.
        /// </summary>
        string Action { get; }
    }
}