﻿using Newtonsoft.Json;

namespace TripAdvisorParser.Core.Data.Api.Commands
{
    public class Command : ICommand
    {
        #region Инициализация

        /// <summary>
        /// Инициализирует комманду.
        /// </summary>
        /// <param name="id">Уникальный ID команды.</param>
        /// <param name="action">Действие</param>
        public Command(string id, string action)
        {
            Id = id;
            Action = action;
        }

        /// <summary>
        /// Инициализирует комманду.
        /// </summary>
        public Command()
        {
            Id = Action = string.Empty;
        }

        #endregion
        
        #region Свойства

        [JsonProperty("mid", Order = 0)] 
        public string Id { get; set; }

        [JsonProperty("action", Order = 1)] 
        public string Action { get; set; }

        #endregion

        #region Типы

        /// <summary>
        /// Тип сортировки.
        /// </summary>
        public enum SortType
        {
            /// <summary>
            /// Сортировка по возрастанию.
            /// </summary>
            Ascending, 
            /// <summary>
            /// Сортировка по убыванию.
            /// </summary>
            Descending
        }

        /// <summary>
        /// Статусы ответа сервера.
        /// </summary>
        public enum Status
        {
            /// <summary>
            /// Команда выполнена успешно
            /// </summary>
            Success           = 1000,
            /// <summary>
            /// Неизвестная ошибка
            /// </summary>
            UnknownError      = 1001,
            /// <summary>
            /// Ошибка авторизации
            /// </summary>
            AuthError         = 1002,
            /// <summary>
            /// Ошибка базы данных
            /// </summary>
            DatabaseError     = 1003,
            /// <summary>
            /// Ошибка запроса
            /// </summary>
            RequestError      = 1005,
            
            /// <summary>
            /// Пользователь не найден в системе
            /// </summary>
            UserNotFound      = 2003,
            /// <summary>
            /// Пользователь заблокирован
            /// </summary>
            UserBlocked       = 2004,
            /// <summary>
            /// Передан неверный токен для данного устройства
            /// </summary>
            InvalidToken      = 2005,
            /// <summary>
            /// Пользователь не активирован на данном устройстве
            /// </summary>
            NotActivatedUser  = 2006
        }


        #endregion
    }
}