﻿using System;
using Newtonsoft.Json;
using TripAdvisorParser.Core.Server.CmdHandler;

namespace TripAdvisorParser.Core.Data.Api.Commands
{
    public class ResponseCommand<T> : StatusCommand
    {
        public ResponseCommand(string id, string action, Status cmdStatus, T result) 
            : base(id, action)
        {
            InitializeCommand(id, action, cmdStatus, result);
        }
        
        public ResponseCommand(string id, string action, T result) 
            : base(id, action)
        {
            InitializeCommand(id, action, Status.Success, result);
        }
        
        public ResponseCommand(string id, Status cmdStatus) 
            : base(id, string.Empty)
        {
            InitializeCommand(id, string.Empty, cmdStatus, default);
        }
        
        public ResponseCommand(Status cmdStatus) 
            : base(string.Empty, string.Empty)
        {
            InitializeCommand(string.Empty, string.Empty, cmdStatus, default);
        }

        /// <summary>
        /// Полностью инициализирует команду.
        /// </summary>
        /// <param name="id">GUID запроса</param>
        /// <param name="action">Действие</param>
        /// <param name="cmdStatus">Статус ответа</param>
        /// <param name="result">Результат</param>
        private void InitializeCommand(string id, string action, Status cmdStatus, T result)
        {
            CmdStatus = cmdStatus;

            if (CmdStatus != Status.Success 
                || string.IsNullOrWhiteSpace(action) 
                || string.IsNullOrWhiteSpace(id))
            {
                return;
            }

            Result = result;
            InputOutput.Write($"Ответ {action} с ID {id} успешно сформирован!", ConsoleColor.DarkGreen);
        }


        public ResponseCommand() : base(string.Empty, string.Empty)
        {
            CmdStatus = Status.UnknownError;
        }

        [JsonProperty("result", Order = 3)] 
        public T Result { get; set; }
    }
}