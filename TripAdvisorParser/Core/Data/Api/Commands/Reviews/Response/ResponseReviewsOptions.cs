﻿using System.Collections.Generic;
using Newtonsoft.Json;
using TripAdvisorParser.Core.Data.Database;
using TripAdvisorParser.Core.Data.Reviews;

namespace TripAdvisorParser.Core.Data.Api.Commands.Reviews.Response
{
    public class ResponseReviewsOptions
    {
        /// <summary>
        /// Инициализирует результат команды, отправляющую отзывы.
        /// </summary>
        /// <param name="reviews"></param>
        /// <param name="lastUpdateTime"></param>
        /// <param name="nextUpdateTime"></param>
        public ResponseReviewsOptions(IList<Review> reviews, string lastUpdateTime, string nextUpdateTime)
        {
            Reviews = reviews;
            LastUpdateTime = lastUpdateTime;
            NextUpdateTime = nextUpdateTime;
        }

        /// <summary>
        /// Инициализирует результат команды, отправляющую отзывы.
        /// </summary>
        public ResponseReviewsOptions()
        {
            Reviews = new List<Review>();
            LastUpdateTime = string.Empty;
        }

        [JsonProperty("reviews")]
        public IList<Review> Reviews { get; set; }
        [JsonProperty("lastupdate")]
        public string LastUpdateTime { get; set; }
        [JsonProperty("nextupdate")]
        public string NextUpdateTime { get; set; }
    }
}