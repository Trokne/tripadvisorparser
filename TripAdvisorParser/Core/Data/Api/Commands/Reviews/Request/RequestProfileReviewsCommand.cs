﻿using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using Newtonsoft.Json;
using TripAdvisorParser.Core.Data.Api.Commands.Reviews.Response;
using TripAdvisorParser.Core.Data.Database;
using TripAdvisorParser.Core.Data.Reviews;
using TripAdvisorParser.Core.Parser;
using TripAdvisorParser.Core.Server;
using TripAdvisorParser.Core.Server.CmdHandler;

namespace TripAdvisorParser.Core.Data.Api.Commands.Reviews.Request
{
    /// <summary>
    /// Команда запроса списка отзывов у профиля заведения
    /// </summary>
    [Description("api.request.profilereviews")]
    public class RequestProfileReviewsCommand : RequestCommand<RequestReviewsOptions>
    {
        /// <summary>
        /// Выполняет запрос из десериализованного объекта.
        /// </summary>
        /// <param name="obj">Объект</param>
        /// <returns>Выполненный запрос в формате Json</returns>
        public static string GetResult(object obj)
        {
            var cmd = JsonConvert.DeserializeObject<RequestProfileReviewsCommand>(obj as string);
            
            if (!ReviewsParser.TryParseProfileReviews(cmd.Options.Guid, out var reviewsGroupId))
            {
                var errorCmd = new ResponseCommand<ResponseReviewsOptions>(cmd.Id, Status.DatabaseError);
                var serializedObject = JsonConvert.SerializeObject(errorCmd);
                return serializedObject;
            }
            
            var reviews = DatabaseHandler
                .GetValues<DbReview>(review => review.ReviewGroupId == reviewsGroupId)
                .Select(review => review.ToJsonReview())
                .ToList();
            
            Review.Filter(ref reviews, cmd.Options.DateSortType, cmd.Options.PageNumber, cmd.Options.PageSize);
            var result = new ResponseReviewsOptions(reviews, 
                ParserServer.Instance.LastUpdateTime.ToShortDateString(),
                ParserServer.Instance.NextUpdateTime.ToShortDateString());
            var response = new ResponseCommand<ResponseReviewsOptions>(cmd.Id, cmd.Action, result);
            var serializedJson = JsonConvert.SerializeObject(response);
            InputOutput.Write($"Сформирован ответ с непустыми отзывами ({reviews.Count} шт.)  на странице {cmd.Options.PageNumber}");
            
            return serializedJson;
        }
    }
}