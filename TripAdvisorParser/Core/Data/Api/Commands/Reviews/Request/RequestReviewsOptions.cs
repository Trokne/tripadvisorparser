﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace TripAdvisorParser.Core.Data.Api.Commands.Reviews.Request
{
    /// <summary>
    /// Дополнительная информация о команде <see cref="RequestGroupReviewsCommand"/>.
    /// </summary>
    public class RequestReviewsOptions
    {
        /// <summary>
        /// GUID группы/профиля заведения.
        /// </summary>
        [JsonProperty("guid")]
        public string Guid { get; set; }

        /// <summary>
        /// Тип сортировки отзывов по дате.
        /// </summary>
        [JsonProperty("sorttype"), 
         JsonConverter(typeof(StringEnumConverter))]
        public Command.SortType DateSortType { get; set; }
        
        /// <summary>
        /// Номер страницы.
        /// </summary>
        [JsonProperty("pagenumber")]
        public int PageNumber { get; set; }
        
        /// <summary>
        /// Размер страницы.
        /// </summary>
        [JsonProperty("pagesize")]
        public int PageSize { get; set; }
    }
}