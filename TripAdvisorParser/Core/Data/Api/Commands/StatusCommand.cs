﻿using Newtonsoft.Json;

namespace TripAdvisorParser.Core.Data.Api.Commands
{
    public class StatusCommand : Command, IResponseCommand
    {
        [JsonProperty("status", Order = 2)] 
        public Status CmdStatus { get; set; }

        public StatusCommand(string id, string action)
        {
            Id = id;
            Action = action;
        }
    }
}