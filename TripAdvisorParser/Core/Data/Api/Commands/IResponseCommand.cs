﻿namespace TripAdvisorParser.Core.Data.Api.Commands
{
    public interface IResponseCommand
    {
        Command.Status CmdStatus { get; set; }
    }
}