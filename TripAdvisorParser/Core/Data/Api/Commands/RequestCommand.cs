﻿using System;
using Newtonsoft.Json;
using TripAdvisorParser.Core.Data.Api.Commands.Reviews.Request;
using TripAdvisorParser.Core.Helpers;

namespace TripAdvisorParser.Core.Data.Api.Commands
{
    public class RequestCommand<T> : Command
    {
        public RequestCommand(T options, string id, string action) 
            : base(id, action)
        {
            Options = options;
        }

        public RequestCommand(T options, string action)
            : base(Guid.NewGuid().ToString(), action)
        {
            Options = options;
        }

        public  RequestCommand()
            : base(Guid.NewGuid().ToString(), null)
        {
            Options = default;
        }

        /// <summary>
        /// Дополнительная информация о запросе.
        /// </summary>
        [JsonProperty("options")]
        public T Options { get; set; }
    }
}