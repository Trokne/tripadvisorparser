﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace TripAdvisorParser.Core.Data.Api.Commands.Url.Response
{
    public class ResponseLinksGroup
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("groupid")]
        public string GroupId { get; set; }
        [JsonProperty("profiles")]
        public List<ResponseLinksProfile> Profiles { get; set; }
    }
}