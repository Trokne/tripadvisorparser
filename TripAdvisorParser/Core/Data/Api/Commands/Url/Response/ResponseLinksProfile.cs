﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace TripAdvisorParser.Core.Data.Api.Commands.Url.Response
{
    public class ResponseLinksProfile
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("address")]
        public string Addreess { get; set; }
        [JsonProperty("tripadvisor")]
        public string Url { get; set; }
    }
}