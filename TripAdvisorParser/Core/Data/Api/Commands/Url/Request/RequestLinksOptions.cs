﻿using Newtonsoft.Json;

namespace TripAdvisorParser.Core.Data.Api.Commands.Url.Request
{
    public class RequestLinksOptions
    {
        public RequestLinksOptions(uint pageNumber, uint pageSize)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
        }

        /// <summary>
        /// Номер страницы.
        /// </summary>
        [JsonProperty("pagenumber")]
        public uint PageNumber { get; set; }
        
        /// <summary>
        /// Размер страницы.
        /// </summary>
        [JsonProperty("pagesize")]
        public uint PageSize { get; set; }
    }
}