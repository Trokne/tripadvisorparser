﻿using System;
using SQLite;

namespace TripAdvisorParser.Core.Data.Database
{
    public class DbGroup : IDatabaseEntity
    {
        [PrimaryKey]
        public string Id { get; set; }
        public string Name { get; set; }
    }
}