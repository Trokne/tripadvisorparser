﻿using System;
using System.Globalization;
using SQLite;
using TripAdvisorParser.Core.Data.Reviews;
using TripAdvisorParser.Core.Server;

namespace TripAdvisorParser.Core.Data.Database
{
    public class DbReview : IDatabaseEntity
    {
        [PrimaryKey]
        public string Id { get; set; }
        public string ReviewGroupId { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }
        public string Image { get; set; }
        public string Date { get; set; }
        public float Rating { get; set; }

        
        /// <summary>
        /// Преобразует значения в формат, удобный для передачи в JSON.
        /// </summary>
        /// <returns>Отзыв в формате, удобном для передачи в JSON.</returns>
        public Review ToJsonReview()
        {
            return new Review
            {
                Date = Date,
                Image = Image,
                Name = Name,
                ReviewGroupId = ReviewGroupId,
                ReviewRating = new Rating
                {
                    RatingValue = Rating,
                    Type = "Rating"
                },
                Text = Text,
                Id = Id
            };
        }
    }
}