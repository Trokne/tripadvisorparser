﻿using Newtonsoft.Json;
using SQLite;

namespace TripAdvisorParser.Core.Data.Database
{
    public class DbReviewsGroup : IDatabaseEntity
    {
        [PrimaryKey]
        public string Id { get; set; }
        public string Url { get; set; }
    }
}