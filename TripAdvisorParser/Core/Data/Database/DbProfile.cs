﻿using System;
using SQLite;

namespace TripAdvisorParser.Core.Data.Database
{
    public class DbProfile : IDatabaseEntity
    {
        [PrimaryKey]
        public string Id { get; set; }
        public string GroupId { get; set; }
        public string ReviewsGroupId { get; set; }
        public string Address { get; set; }

        public DbProfile(string id)
        {
            Id = id;
        }
        
        public DbProfile()
        {
            Id = GroupId = ReviewsGroupId = Address = null;
        }
    }
}