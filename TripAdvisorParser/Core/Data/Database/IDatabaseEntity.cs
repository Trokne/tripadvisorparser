﻿using SQLite;

namespace TripAdvisorParser.Core.Data.Database
{
    public interface IDatabaseEntity
    {
        string Id { get; set; }
    }
}