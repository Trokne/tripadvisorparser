﻿using System;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using TripAdvisorParser.Core.Data;
using TripAdvisorParser.Core.Data.Api;
using TripAdvisorParser.Core.Data.Parameters;
using TripAdvisorParser.Core.Parser;
using TripAdvisorParser.Core.Server;
using TripAdvisorParser.Core.Server.CmdHandler;

namespace TripAdvisorParser.Core
{
    internal static class Program
    {
        public static async Task Main()
        {
            InputOutput.SetEncoding(Encoding.UTF8);
            
            InputOutput.Write($"Настройки будут загружены из файла {Settings.Instance.General.ParamsName}.");
            
            if (Settings.Instance.TryLoadFile())
            {
                InputOutput.Write("Настройки успешно загружены!", ConsoleColor.DarkGreen);
            }

            await ParserServer.Instance.StartServer();
        }
    }
}